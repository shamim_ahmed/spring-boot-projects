package edu.buet.cse.service;

import java.util.Date;

import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
import org.springframework.stereotype.Service;

@Service
public class StubHumanResourceService implements HumanResourceService {
  private final Logger logger = LogManager.getLogger(getClass());
  
  @Override
  public void bookHoliday(Date startDate, Date endDate, String name) {
    logger.info(String.format("Booking holiday for %s from %s to %s", name, startDate, endDate));
  }
}
