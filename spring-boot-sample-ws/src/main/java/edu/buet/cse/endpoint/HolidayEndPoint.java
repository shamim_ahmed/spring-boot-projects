package edu.buet.cse.endpoint;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

import org.jdom2.Element;
import org.jdom2.Namespace;
import org.jdom2.filter.Filters;
import org.jdom2.xpath.XPathExpression;
import org.jdom2.xpath.XPathFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.ws.server.endpoint.annotation.Endpoint;
import org.springframework.ws.server.endpoint.annotation.PayloadRoot;
import org.springframework.ws.server.endpoint.annotation.RequestPayload;

import edu.buet.cse.service.HumanResourceService;

@Endpoint
public class HolidayEndPoint {
  private static final String NAMESPACE_URI = "http://mycompany.com/hr/schemas";
  
  private final HumanResourceService humanResourceService;
  private final XPathExpression<Element> startDateExpression;
  private final XPathExpression<Element> endDateExpression;
  private final XPathExpression<String> nameExpression;
  
  @Autowired
  public HolidayEndPoint(HumanResourceService humanResourceService) {
    this.humanResourceService = humanResourceService;
    
    Namespace namespace = Namespace.getNamespace("hr", NAMESPACE_URI);
    XPathFactory xpathFactory = XPathFactory.instance();
    startDateExpression = xpathFactory.compile("//hr:StartDate", Filters.element(), null, namespace);
    endDateExpression = xpathFactory.compile("//hr:EndDate", Filters.element(), null, namespace);
    nameExpression = xpathFactory.compile("concat(//hr:FirstName, ' ', //hr:LastName)", Filters.fstring(), null, namespace);
  }
  
  @PayloadRoot(namespace = NAMESPACE_URI, localPart = "HolidayRequest")
  public void handleHolidayRequest(@RequestPayload Element holidayRequest) {
    DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
    Date startDate = null;
    Date endDate = null;
    
    Element startDateElement = startDateExpression.evaluateFirst(holidayRequest);
    
    if (startDateElement != null) {
      try {
        startDate = dateFormat.parse(startDateElement.getText());
      } catch (ParseException ex) {
        ex.printStackTrace(System.err);
      }
    }
    
    Element endDateElement = endDateExpression.evaluateFirst(holidayRequest);
    
    if (endDateElement != null) {
      try {
        endDate = dateFormat.parse(endDateElement.getText());
      } catch (ParseException ex) {
        ex.printStackTrace(System.err);
      }
    }
    
    String name = nameExpression.evaluateFirst(holidayRequest);
    
    // invoke the service with the retrieved data from incoming message
    humanResourceService.bookHoliday(startDate, endDate, name);
  }
}
