package edu.buet.cse.controller;

import java.util.concurrent.atomic.AtomicLong;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import edu.buet.cse.model.Greeting;

@RestController
public class GreetingController {
  private static final String GREETING_TEMPLATE = "Hello, %s !";
  private final AtomicLong counter = new AtomicLong(0);
  
  @RequestMapping(value = "/greet", method = RequestMethod.GET)
  public Greeting getGreeting(@RequestParam(value="name", defaultValue="Anon") String name) {
    String message = String.format(GREETING_TEMPLATE, name);
    Greeting greeting = new Greeting(counter.incrementAndGet(), message);
    return greeting;
  }
}
