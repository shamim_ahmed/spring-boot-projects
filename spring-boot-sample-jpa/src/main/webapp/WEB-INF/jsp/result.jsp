<%@ page language="java" contentType="text/html; charset=UTF-8" %>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<html>
<head>
  <meta charset="UTF-8"/>
  <title>Spring Boot Demo</title>
</head>
<body>
  <h1>Spring Boot Demo</h1>
  <h2>The notes</h2>
  
  <ul>
    <c:forEach var="note" items="${notes}">
      <li>${note.title}</li>
    </c:forEach>
  </ul>
</body>
</html>