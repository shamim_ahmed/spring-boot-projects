package edu.buet.cse.web;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import edu.buet.cse.domain.Note;
import edu.buet.cse.repository.NoteRepository;

@Controller
@Transactional(readOnly = true)
public class IndexController {
  @Autowired
  private NoteRepository noteRepository;

  @RequestMapping(value = "/")
  public ModelAndView index() {
    ModelAndView modelView = new ModelAndView();
    List<Note> notes = noteRepository.findAll();
    modelView.addObject("notes", notes);
    modelView.setViewName("result");
    
    return modelView;
  }
}
