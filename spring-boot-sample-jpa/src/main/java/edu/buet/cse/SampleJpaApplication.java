package edu.buet.cse;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.boot.context.web.SpringBootServletInitializer;

@SpringBootApplication
public class SampleJpaApplication extends SpringBootServletInitializer {
  
  @Override
  public SpringApplicationBuilder configure(SpringApplicationBuilder application) {
    return application.sources(SampleJpaApplication.class);
  }

  public static void main(String[] args) {
    SpringApplication.run(SampleJpaApplication.class, args);
  }
}
