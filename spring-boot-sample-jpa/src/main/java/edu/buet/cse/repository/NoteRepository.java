package edu.buet.cse.repository;

import java.util.List;

import edu.buet.cse.domain.Note;

public interface NoteRepository {
  List<Note> findAll();
}
