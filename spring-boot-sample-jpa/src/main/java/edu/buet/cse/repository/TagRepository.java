package edu.buet.cse.repository;

import java.util.List;

import edu.buet.cse.domain.Tag;

public interface TagRepository {
  List<Tag> findAll();
}
