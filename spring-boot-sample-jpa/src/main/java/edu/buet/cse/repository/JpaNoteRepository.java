package edu.buet.cse.repository;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;

import org.springframework.stereotype.Repository;

import edu.buet.cse.domain.Note;

@Repository
public class JpaNoteRepository implements NoteRepository {
  @PersistenceContext
  private EntityManager entityManager;

  @Override
  public List<Note> findAll() {
    TypedQuery<Note> query = entityManager.createQuery("select n from Note n", Note.class);
    return query.getResultList();
  }
}
