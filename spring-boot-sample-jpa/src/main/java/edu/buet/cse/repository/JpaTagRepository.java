package edu.buet.cse.repository;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;

import org.springframework.stereotype.Repository;

import edu.buet.cse.domain.Tag;

@Repository
public class JpaTagRepository implements TagRepository {
  @PersistenceContext
  private EntityManager entityManager;
  
  @Override
  public List<Tag> findAll() {
    TypedQuery<Tag> query = entityManager.createQuery("select t from Tag t", Tag.class);
    return query.getResultList();
  }
}
