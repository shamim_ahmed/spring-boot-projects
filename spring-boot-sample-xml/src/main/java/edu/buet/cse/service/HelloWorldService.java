package edu.buet.cse.service;

import org.springframework.stereotype.Service;

@Service
public class HelloWorldService {
  public void sayHello() {
    System.out.println("Hello world !!");
  }
}
