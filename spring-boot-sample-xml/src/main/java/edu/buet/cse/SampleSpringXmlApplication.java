package edu.buet.cse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;

import edu.buet.cse.service.HelloWorldService;

public class SampleSpringXmlApplication implements  CommandLineRunner {
  @Autowired
  private HelloWorldService helloWorldService;

  @Override
  public void run(String... args) throws Exception { 
    helloWorldService.sayHello();
  }
  
  public static void main(String[] args) {
    SpringApplication.run("classpath:application-context.xml", args);
  }
}
