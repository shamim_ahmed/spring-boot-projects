package edu.buet.cse;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ApplicationContext;

@SpringBootApplication
public class HelloWorldApp {
  
  public static void main(String... args) {
    ApplicationContext appContext = SpringApplication.run(HelloWorldApp.class, args);
    String[] beanNameArray = appContext.getBeanDefinitionNames();
    
    System.out.println("Registered bean names: ");
    
    for (String beanName : beanNameArray) {
      System.out.println(beanName);
    }
  }
}
