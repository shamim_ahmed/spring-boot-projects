package edu.buet.cse.controller;

import org.springframework.boot.*;
import org.springframework.web.bind.annotation.*;

@RestController
public class SimpleController {

  @RequestMapping("/")
  String home() {
    return "Hello Spring Boot !";
  }

  public static void main(String[] args) throws Exception {
    SpringApplication.run(SimpleController.class, args);
  }
}
