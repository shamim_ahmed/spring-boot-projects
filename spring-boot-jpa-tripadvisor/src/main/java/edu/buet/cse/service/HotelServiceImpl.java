package edu.buet.cse.service;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import edu.buet.cse.domain.Hotel;
import edu.buet.cse.domain.Review;
import edu.buet.cse.domain.ReviewDetails;

@Service
public class HotelServiceImpl implements HotelService {
  private final HotelRepository hotelRepository;
  
  @Autowired
  public HotelServiceImpl(HotelRepository hotelRepository) {
    this.hotelRepository = hotelRepository;
  }
  
  @Override
  public Hotel getHotel(long cityId, String name) {
    return hotelRepository.findByCityIdAndName(cityId, name);
  }

  @Override
  public List<String> getReviews(long hotelId) {
    List<Review> reviewList = hotelRepository.findReviewsByHotelId(hotelId);
    List<String> resultList = new ArrayList<>();
    
    for (Review r : reviewList) {
      resultList.add(r.getDetails());
    }
    
    return resultList;
  }

  @Override
  public Review addReview(long hotelId, ReviewDetails reviewDetails) {
    return hotelRepository.addReviewByHotelId(hotelId, reviewDetails);
  }
}
