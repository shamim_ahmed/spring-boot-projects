package edu.buet.cse.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import edu.buet.cse.domain.City;

@Service
public class CityServiceImpl implements CityService {
  
  private final CityRepository cityRepository;
  
  @Autowired
  public CityServiceImpl(CityRepository cityRepository) {
    this.cityRepository = cityRepository;
  }

  @Override
  public City getCity(String name, String country) {
    return cityRepository.findByNameAndCountryIgnoringCase(name, country);
  }

  @Override
  public List<City> getAllCities() {
    return cityRepository.findAll();
  }
}
