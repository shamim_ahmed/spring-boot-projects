package edu.buet.cse.service;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;

import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
import org.springframework.stereotype.Repository;
import org.springframework.util.Assert;

import edu.buet.cse.domain.Hotel;
import edu.buet.cse.domain.Review;
import edu.buet.cse.domain.ReviewDetails;

@Repository
public class HotelRepositoryImpl implements HotelRepository {
  private final Logger logger = LogManager.getLogger(getClass());

  @PersistenceContext
  private EntityManager entityManager;

  @Override
  public List<Hotel> findByCityId(long cityId) {
    TypedQuery<Hotel> query = entityManager.createNamedQuery("hotel.findByCity", Hotel.class);
    query.setParameter(1, cityId);
    return query.getResultList();
  }

  @Override
  public Hotel findByCityIdAndName(long cityId, String name) {
    TypedQuery<Hotel> query = entityManager.createNamedQuery("hotel.findByCityAndName", Hotel.class);
    query.setParameter(1, cityId);
    query.setParameter(2, name);
    Hotel result = null;

    try {
      result = query.getSingleResult();
    } catch (Exception ex) {
      logger.error(ex);
    }

    return result;
  }

  @Override
  public Hotel findByHotelId(long hotelId) {
    TypedQuery<Hotel> query = entityManager.createQuery("select h from Hotel h where h.id = ?1", Hotel.class);
    query.setParameter(1, hotelId);
    
    Hotel result = null;
    
    try {
      result = query.getSingleResult();
    } catch (Exception ex) {
      logger.error(ex);
    }
    
    return result;
  }

  @Override
  public Review addReviewByHotelId(long hotelId, ReviewDetails reviewDetails) {
    Hotel hotel = findByHotelId(hotelId);
    Assert.notNull(hotel);
    Review review = new Review(hotel, reviewDetails);
    hotel.getReviews().add(review);
    
    entityManager.persist(review);
    entityManager.flush();
    return review;
  }

  @Override
  public List<Review> findReviewsByHotelId(long hotelId) {
    TypedQuery<Review> query = entityManager.createQuery("select r from Review r where r.hotel.id = ?1", Review.class);
    query.setParameter(1, hotelId);
    return query.getResultList();
  }
}
