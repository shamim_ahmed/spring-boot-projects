package edu.buet.cse.service;

import java.util.List;

import edu.buet.cse.domain.City;

public interface CityRepository {
  List<City> findAll();
  City findByNameAndCountryIgnoringCase(String name, String country);
}
