package edu.buet.cse.service;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;

import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
import org.springframework.stereotype.Repository;
import org.springframework.util.Assert;

import edu.buet.cse.domain.City;

@Repository
public class CityRepositoryImpl implements CityRepository {
  @PersistenceContext
  private EntityManager entityManager;

  private final Logger logger = LogManager.getLogger(getClass());

  @Override
  public List<City> findAll() {
    TypedQuery<City> query = entityManager.createNamedQuery("city.findAll", City.class);
    return query.getResultList();
  }

  @Override
  public City findByNameAndCountryIgnoringCase(String name, String country) {
    Assert.notNull(name);
    Assert.notNull(country);
    TypedQuery<City> query = entityManager.createNamedQuery("city.findByNameAndCountry", City.class);
    query.setParameter(1, name.toLowerCase());
    query.setParameter(2, country.toLowerCase());
    City result = null;

    try {
      result = query.getSingleResult();
    } catch (Exception ex) {
      logger.error(ex);
    }

    return result;
  }
}
