package edu.buet.cse.service;

import java.util.List;

import edu.buet.cse.domain.City;

public interface CityService {
  City getCity(String name, String country);
  List<City> getAllCities(); 
}
