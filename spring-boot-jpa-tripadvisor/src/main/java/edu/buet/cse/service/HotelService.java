package edu.buet.cse.service;

import java.util.List;

import edu.buet.cse.domain.Hotel;
import edu.buet.cse.domain.Review;
import edu.buet.cse.domain.ReviewDetails;

public interface HotelService {
  Hotel getHotel(long cityId, String name);
  List<String> getReviews(long hotelId);
  Review addReview(long hotelId, ReviewDetails reviewDetails);
}
