package edu.buet.cse.service;

import java.util.List;

import edu.buet.cse.domain.Hotel;
import edu.buet.cse.domain.Review;
import edu.buet.cse.domain.ReviewDetails;

public interface HotelRepository {
  List<Hotel> findByCityId(long cityId);
  Hotel findByCityIdAndName(long cityId, String name);
  Hotel findByHotelId(long hotelId);
  List<Review> findReviewsByHotelId(long hotelId);
  Review addReviewByHotelId(long hotelId, ReviewDetails review);
}
