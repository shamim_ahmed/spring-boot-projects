package edu.buet.cse;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class JpaTripAdvisorApplication {

  public static void main(String[] args) {
    SpringApplication.run(JpaTripAdvisorApplication.class, args);
  }
}
