package edu.buet.cse.domain;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
public class Review implements Serializable {
  private static final long serialVersionUID = 1L;

  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  private Long id;

  @ManyToOne(optional = false)
  private Hotel hotel;

  @Column(nullable = false)
  @Enumerated(EnumType.ORDINAL)
  private Rating rating;

  @Column(nullable = false)
  @Temporal(TemporalType.DATE)
  private Date checkInDate;

  @Column(nullable = false)
  @Enumerated(EnumType.ORDINAL)
  private TripType tripType;

  @Column(nullable = false)
  private String title;

  @Column(nullable = false, length = 5000)
  private String details;

  public Review() {
  }

  public Review(Hotel hotel, ReviewDetails reviewDetails) {
    this.hotel = hotel;
    this.rating = reviewDetails.getRating();
    this.checkInDate = reviewDetails.getCheckInDate();
    this.tripType = reviewDetails.getTripType();
    this.title = reviewDetails.getTitle();
    this.details = reviewDetails.getDetails();
  }

  public Long getId() {
    return id;
  }

  public void setId(Long id) {
    this.id = id;
  }
  
  public Hotel getHotel() {
    return hotel;
  }

  public void setHotel(Hotel hotel) {
    this.hotel = hotel;
  }

  public Rating getRating() {
    return rating;
  }

  public void setRating(Rating rating) {
    this.rating = rating;
  }

  public Date getCheckInDate() {
    return checkInDate;
  }

  public void setCheckInDate(Date checkInDate) {
    this.checkInDate = checkInDate;
  }

  public TripType getTripType() {
    return tripType;
  }

  public void setTripType(TripType tripType) {
    this.tripType = tripType;
  }

  public String getTitle() {
    return title;
  }

  public void setTitle(String title) {
    this.title = title;
  }

  public String getDetails() {
    return details;
  }

  public void setDetails(String details) {
    this.details = details;
  }
}
