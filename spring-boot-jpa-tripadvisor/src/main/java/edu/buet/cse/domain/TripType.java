package edu.buet.cse.domain;

public enum TripType {
  BUSINESS, COUPLES, FAMILY, FRIENDS, SOLO
}
