package edu.buet.cse.domain;

public enum Rating {
  TERRIBLE, POOR, AVERAGE, GOOD, EXCELLENT
}
