package edu.buet.cse.domain;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;

@Entity
@NamedQueries({ @NamedQuery(name = "hotel.findByCity", query = "select h from Hotel h where h.city.id = ?1"),
  @NamedQuery(name = "hotel.findByCityAndName", query = "select h from Hotel h where h.city.id = ?1 and h.name = ?2")})
public class Hotel implements Serializable {
  private static final long serialVersionUID = 1L;
  
  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  private Long id;

  @ManyToOne(optional = false)
  private City city;
  
  @Column(nullable = false)
  private String name;
  
  @Column(nullable = false)
  private String address;
  
  @Column(nullable = false)
  private String zip;
  
  @OneToMany(fetch = FetchType.LAZY, mappedBy = "hotel")
  private List<Review> reviews;
  
  public Long getId() {
    return id;
  }

  public void setId(Long id) {
    this.id = id;
  }

  public City getCity() {
    return city;
  }

  public void setCity(City city) {
    this.city = city;
  }

  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  public String getAddress() {
    return address;
  }

  public void setAddress(String address) {
    this.address = address;
  }

  public String getZip() {
    return zip;
  }

  public void setZip(String zip) {
    this.zip = zip;
  }

  public List<Review> getReviews() {
    if (reviews == null) {
      reviews = new ArrayList<>();
    }
    
    return reviews;
  }

  public void setReviews(List<Review> reviews) {
    this.reviews = reviews;
  }
}
