package edu.buet.cse.web;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import edu.buet.cse.domain.City;
import edu.buet.cse.service.CityService;

@RestController
@Transactional(readOnly = true)
public class CityController {
  @Autowired
  private CityService cityService;

  @RequestMapping(value = "/cities")
  public List<City> getAllCities() {
    List<City> cityList = cityService.getAllCities();
    return cityList;
  }

  @RequestMapping(value = "/city")
  public City getCity(@RequestParam String name, @RequestParam String country) {
    return cityService.getCity(name, country);
  }
}
