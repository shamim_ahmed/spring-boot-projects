package edu.buet.cse.web;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.Assert;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import edu.buet.cse.domain.Hotel;
import edu.buet.cse.domain.Review;
import edu.buet.cse.domain.ReviewDetails;
import edu.buet.cse.service.HotelService;

@RestController
@Transactional
public class HotelController {
  @Autowired
  private HotelService hotelService;
  
  @RequestMapping(value = "/hotel", method = RequestMethod.GET)
  public Hotel getHotel(@RequestParam long cityId, @RequestParam String name) {
    return hotelService.getHotel(cityId, name);
  }
  
  @RequestMapping(value = "/hotel/reviews", method = RequestMethod.GET)
  public List<String> getReviews(@RequestParam long hotelId) {
    return hotelService.getReviews(hotelId);
  }
  
  @RequestMapping(value = "/hotel/review", method = RequestMethod.POST)
  public long addReview(@RequestParam long hotelId, @RequestBody ReviewDetails reviewDetails) {
    Review review = hotelService.addReview(hotelId, reviewDetails);
    Assert.notNull(review);
    return review.getId();
  }
}
