package edu.buet.cse.controller;

import java.util.Date;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import edu.buet.cse.service.GreetingService;

@Controller
public class MessageController {
  @Autowired
  private GreetingService greetingService;

  @RequestMapping(value = "/")
  public String getMessage(@RequestParam String name, Map<String, Object> model) {
    String message = greetingService.getGreeting(name);
    model.put("result", message);
    model.put("currentTime", new Date());
    return "message";
  }
}
