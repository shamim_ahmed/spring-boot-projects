<%@ page language="java" contentType="text/html; charset=UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<html>
<head>
  <meta charset="UTF-8"/>
  <title>Spring Boot Demo</title>
</head>
<body>
  <h1>Spring Boot Demo</h1>
  <p>The result is <c:out value="${result}"/></p>
  <p>The current time is <c:out value="${currentTime}"/></p>
</body>
</html>