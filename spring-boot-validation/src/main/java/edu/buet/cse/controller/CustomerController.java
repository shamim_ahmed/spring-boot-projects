package edu.buet.cse.controller;

import javax.validation.Valid;

import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import edu.buet.cse.model.Customer;
import edu.buet.cse.model.Status;

@RestController
public class CustomerController {
  protected final Logger logger = LogManager.getLogger(getClass());

  @RequestMapping(value = "/signup", method = RequestMethod.POST)
  @ResponseBody
  public Status createCustomer(@RequestBody @Valid Customer customer, BindingResult result) {
    Status status = new Status();

    if (result.hasErrors()) {
      status.setMessage("failure");
      logger.error("The validation errors are: " + result.getAllErrors());
    } else {
      // save the customer in database etc.
      logger.info("The parsed customer data: " + customer);
      status.setMessage("success");
    }

    return status;
  }
}
