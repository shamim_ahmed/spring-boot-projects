package edu.buet.cse;

import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import edu.buet.cse.service.MessageService;

@Configuration
public class BeanDefinitions {

  @Bean
  public MessageService messageService(ApplicationContext applicationContext) {
    return new MessageService();
  }
}
