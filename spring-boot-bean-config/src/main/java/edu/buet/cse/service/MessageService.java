package edu.buet.cse.service;

import edu.buet.cse.domain.Customer;

public class MessageService {
  
  public String getGreeting(Customer customer) {
    return String.format("Hello, %s %s !", customer.getFirstName(), customer.getLastName());
  }
}
