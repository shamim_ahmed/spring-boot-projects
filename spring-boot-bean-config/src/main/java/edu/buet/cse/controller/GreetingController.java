package edu.buet.cse.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import edu.buet.cse.domain.Customer;
import edu.buet.cse.domain.Greeting;
import edu.buet.cse.service.MessageService;

@RestController
public class GreetingController {
  @Autowired
  private MessageService messageService;
  
  @RequestMapping("/greeting")
  public Greeting getGreeting(@RequestBody Customer customer) {
    Greeting result = new Greeting();
    result.setMessage(messageService.getGreeting(customer));
    return result;
  }
}
